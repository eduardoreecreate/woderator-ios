//
//  DataService.swift
//  codder-swag
//
//  Created by ReeCreate on 04/04/2018.
//  Copyright © 2018 ReeCreate. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()
    
    private let categories = [
        Category(title: "SHIRTS", imageName: "shirts.png"),
        Category(title: "HOODIES", imageName: "hoodies.png"),
        Category(title: "HATS", imageName: "hats.png"),
        Category(title: "DIGITAL", imageName: "digital.png")
    ]
    
    
    private let hats =  [
        Product(title: "Devslopes Logo Beanie", price: "£18", imageName: "hat01.png"),
        Product(title: "Devslopes Logo Hat", price: "£10", imageName: "hat02.png"),
        Product(title: "Devslopes Logo White", price: "£20", imageName: "hat03.png"),
        Product(title: "Devslopes Logo Snapback", price: "£22", imageName: "hat04.png")
    ]
    
    private let hoodies = [
        Product(title: "Devslopes Hoodie Grey", price: "£32", imageName: "hoodie01.png"),
        Product(title: "Devslopes Hoodie Red", price: "£30", imageName: "hoodie02.png"),
        Product(title: "Devslopes Hoodie Black", price: "£35", imageName: "hoodie03.png"),
        Product(title: "Devslopes Hoodie Blue", price: "£42", imageName: "hoodie04.png")
    ]
    
    private let shirts = [
        Product(title: "Devslopes Shirt Black", price: "£18", imageName: "shirt01.png"),
        Product(title: "Devslopes Shirt Grey", price: "£15", imageName: "shirt02.png"),
        Product(title: "Devslopes Shirt Red", price: "£20", imageName: "shirt03.png"),
        Product(title: "Devslopes Shirt Blue", price: "£10", imageName: "shirt04.png")
    ]
    
    private let digitalGoods = [Product]()
    
    func getCategories() -> [Category] {
        return categories
    }
    
    func getProducts(forCategoryTitle title: String) -> [Product] {
        switch title {
        case "SHIRTS":
            return getShirts()
        case "HATS":
            return getHats()
        case "HOODIES":
            return getHoodies()
        case "DIGITAL":
            return getDigitalGoods()
        default:
            return getShirts()
        }
    }
    
    func getHats() -> [Product] {
        return hats
        
    }
    
    func getHoodies() -> [Product] {
        return hoodies
    }
    
    func getShirts() -> [Product] {
        return shirts
    }
    
    func getDigitalGoods() -> [Product] {
        return digitalGoods
    }
}
